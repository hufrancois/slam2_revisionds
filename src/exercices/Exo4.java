
package exercices;

import data.Dao;
import entites.Region;

public class Exo4 {

    public static void main(String[] args) {
        
        System.out.println("Liste des régions : ");
        for(Region reg : Dao.getToutesLesRegions()){
            System.out.println();
            System.out.printf("%-5s %-15s %4d", reg.getCodeRegion(), reg.getNomRegion(), reg.getLesClients().size());
                    
            
        }
    }
}

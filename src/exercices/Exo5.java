
package exercices;

import data.Dao;
import entites.Facture;
import utilitaires.UtilDate;

public class Exo5 {

    public static void main(String[] args) {
        
        System.out.println("Facture de Janvier 2018 non réglées à la date du : "+UtilDate.aujourdhuiChaine());
        for(Facture f : Dao.getToutesLesFactures()){
            
            int anF = UtilDate.annee(f.getDateFact());
            int moisF = UtilDate.mois(f.getDateFact());
            
            if(anF == 2018 && moisF == 1 && !f.getReglee()){
                System.out.println(f.getNumFact()+" "+
                                   UtilDate.dateVersChaine(f.getDateFact())+" "+
                                   f.getLeClient().getNomCli()+" "+
                                   f.getLeClient().getAdrCli()+" "+
                                   f.getLeClient().getLaRegion().getNomRegion()+" "+
                                   f.getMontantFact()+" €"
                        );
                
            }
            
            
        }
    }
}

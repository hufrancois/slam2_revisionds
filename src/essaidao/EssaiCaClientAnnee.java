package essaidao;
import data.Dao;
import entites.Client;
import entites.Facture;

public class EssaiCaClientAnnee {

    
    public static void main(String[] args) {
        
        
        Client c = Dao.getClientDeNumero(101L);

        System.out.println();
        
        System.out.println(" Client: " +c.getNumCli());
        System.out.println(" Nom: "    +c.getNomCli());
        System.out.println(" Adresse:" +c.getAdrCli());
        System.out.println(" Region: " +c.getLaRegion().getNomRegion());
        
        System.out.println();
        
    }
}
